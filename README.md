### Rzeczy, które mógłbym poprawić ###

- Dodanie Proptypes
- Wydzielenie połączenia komponentów z redux do osobnych plików (choć przy tak małym projekcie nie jest to kluczowe)
- Rozbicie akcji i reduktorów na mniejsze części (choć przy tak małym projekcie nie jest to kluczowe)
- Lepsza obsługa błędów
- Przeniesienie części rzeczy do backendu - w bieżącej implementacji backend wystawia tylko API do zapisu do bazy. Przykładowo, można wykorzystać JMS Serializer do formatowania danych wyjściowych, zamiast obcinać ułąmki po stronie front.
- UnitTest