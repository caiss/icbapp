import axios from 'axios';

// could be injected from ENV
const API_URL = 'http://icbapi.dev.gotoma.pl:10600/api/';

const OPENWEATHER_URL = 'http://api.openweathermap.org/data/2.5/weather?';
const OPENWEATHER_KEY = '739f64dff99e662803ec9a6f6445af8d';

const KELVIN_DIFF = -272.15;

export const REQUEST_WEATHER_DATA = 'REQUEST_WEATHER_DATA';
export const RECEIVE_WEATHER_DATA = 'RECEIVE_WEATHER_DATA';
export const RECEIVE_WEATHER_DATA_ERROR = 'RECEIVE_WEATHER_DATA_ERROR';
export const REQUEST_SAVE_DATA = 'REQUEST_SAVE_DATA';
export const RESPONSE_SAVED_DATA = 'RESPONSE_SAVED_DATA';
export const RESPONSE_SAVED_DATA_ERROR = 'RESPONSE_SAVED_DATA_ERROR';
export const REQUEST_HISTORY_DATA = 'REQUEST_HISTORY_DATA';
export const RECEIVE_HISTORY_DATA = 'RECEIVE_HISTORY_DATA';
export const RECEIVE_HISTORY_DATA_ERROR = 'RECEIVE_HISTORY_DATA_ERROR';
export const SHOW_RESULT_MODAL = 'SHOW_RESULT_MODAL';
export const CLOSE_RESULT_MODAL = 'CLOSE_RESULT_MODAL';

/* OpenWeahter */

export const requestWeatherData = () => ({
  type: REQUEST_WEATHER_DATA,
});

export const receiveWeatherData = payload => ({
  type: RECEIVE_WEATHER_DATA,
  payload,
});

export const receiveWeatherDataError = error => ({
  type: RECEIVE_WEATHER_DATA_ERROR,
  error,
});

export const getWeatherData = (lat, lon) => (dispatch, getState) => {

  if (getState().openweather.fetching) {
    return null;
  }

  dispatch(requestWeatherData());

  const startTime = performance.now();

  axios.get(OPENWEATHER_URL + `lat=${lat}&lon=${lon}&appid=${OPENWEATHER_KEY}`)
  .then(
    res => {
      const endTime = performance.now();

      return dispatch(receiveWeatherData({
        data: res.data,
        lat,
        lon,
        lastFetch: new Date(),
        lookup: endTime - startTime
      }));
    },
    error => dispatch(receiveWeatherDataError(error))
  )
  .then(res => dispatch(postSaveData(res.payload)))
  .then(res => dispatch(showResultModal(res)));
};

/* Save To API */

export const requestSaveData = () => ({
  type: REQUEST_SAVE_DATA,
});

export const responseSavedData = () => ({
  type: RESPONSE_SAVED_DATA,
});

export const responseSavedDataError = error => ({
  type:  RESPONSE_SAVED_DATA_ERROR,
  error,
});

export const postSaveData = payload => dispatch => {

  dispatch(requestSaveData());

  const outputData = {
    lon: payload.lon,
    lat: payload.lat,
    city: payload.data.name,
    lookup: payload.lookup,
    temp: payload.data.main.temp + KELVIN_DIFF,
    clouds: payload.data.clouds.all,
    wind: payload.data.wind.speed,
    description: Array.isArray(payload.data.weather) && payload.data.weather.length ?
      payload.data.weather[0].description : null
  };

  axios({
    method: 'post',
    url: API_URL + 'entry',
    data: outputData,
  }).then(
    res => dispatch(responseSavedData(res)),
    error => dispatch(responseSavedDataError(error))
  );

  return outputData;
};

/* Get From API */

export const requestHistoryData = () => ({
  type: REQUEST_HISTORY_DATA,
});

export const responseHistoryData = payload => ({
  type: RECEIVE_HISTORY_DATA,
  payload,
});

export const responseHistoryDataError = error => ({
  type:  RECEIVE_HISTORY_DATA_ERROR,
  error,
});

export const getHistoryData = () => dispatch => {

  dispatch(requestHistoryData());

  axios({
    method: 'get',
    url: API_URL + 'entry',
  }).then(
    res => dispatch(responseHistoryData(res)),
    error => dispatch(responseHistoryDataError(error))
  );
};


/* Modal */

export const showResultModal = payload => ({
  type: SHOW_RESULT_MODAL,
  payload
});

export const closeResultModal = () => ({
  type: CLOSE_RESULT_MODAL
});

