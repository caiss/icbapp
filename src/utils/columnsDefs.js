const columnsDefs = [
  ['lon', 'Longitude', d => d.lon ? d.lon.toFixed(7) : 0],
  ['lat', 'Latitude', d => d.lat ? d.lat.toFixed(7) : 0],
  ['city', 'City', d => d.city],
  ['lookup', 'Lookup time', d => d.lookup ? (d.lookup / 1000).toFixed(6): 0],
  ['temp', 'Temperature', d => d.temp ? d.temp.toFixed(2): 0],
  ['clouds', 'Overcast', d => d.clouds],
  ['wind', 'Wind', d => d.wind],
  ['description', 'Description', d => d.description],
];

export default columnsDefs;