import React, { Component } from 'react';
import ReactTable from 'react-table'
import "react-table/react-table.css";
import Stats from './Stats';
import { getHistoryData } from '../../actions/actions'
import columnsDefs from '../../utils/columnsDefs';
import { connect } from 'react-redux';

class EntriesList extends Component {

  componentDidMount () {
    this.props.loadListData();
  }

  render () {
    const columns = columnsDefs.map(([id, Header, accessor]) => ({ id, Header, accessor }));

    return (
      <>
        <Stats data={this.props.data} />
        <ReactTable
          data={this.props.data}
          columns={columns}
          defaultPageSize={10}
        />
      </>
    );
  }
}

/* CONNECT */

const mapStateToProps = state => ({
  data: state.entries.data
});

const mapDispatchToProps = dispatch => ({
  loadListData: () => dispatch(getHistoryData())
});

export default connect(mapStateToProps, mapDispatchToProps)(EntriesList);