import React from 'react';
import './Stats.scss';

const Stats = props => {

  const temperatures = props.data.map(entry => entry.temp);
  const cities = props.data.map(entry => entry.city);

  const minTemp = temperatures.length ? Math.min(...temperatures) : 0;
  const maxTemp = temperatures.length ? Math.max(...temperatures) : 0;
  const avgTemp = !temperatures.length ? 0 : temperatures.reduce((p, c) => p + c, 0) / temperatures.length;
  const freqCity = cities.sort((a, b) => cities.filter(c => c === a).length - cities.filter(c => c === b).length).pop();
  const entries = props.data.length;

  const rows = [
    ['Min. temperature', minTemp.toFixed(2)],
    ['Max. temperature', maxTemp.toFixed(2)],
    ['Avg. temperature', avgTemp.toFixed(3)],
    ['Most frequent city', freqCity],
    ['Entries count', entries],
  ];

  const tbody = rows.map(([key, value]) => (
    <tr key={key}>
      <th>{ key }</th>
      <td>{ value }</td>
    </tr>
  ));

  return (
    <div className="Stats">
      <table>
        <tbody>
          { tbody }
        </tbody>
      </table>
    </div>
  );
};

export default Stats;