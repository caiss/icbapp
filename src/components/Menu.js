import React from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.scss';

const Menu = () => {
  return (
    <nav className="Menu">
      <ul>
        <li>
          <NavLink exact to="/"> Google Map</NavLink>
        </li>
        <li>
          <NavLink exact to="/list">History</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Menu;