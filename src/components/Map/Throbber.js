import React from 'react';
import './Throbber.scss';
import { ReactComponent as Loader } from '../../assets/loader.svg';
import { connect } from "react-redux";

const Throbber = props => props.visible && <Loader className="Throbber" />;

/* CONNECT */

const mapStateToProps = state => ({
  visible: state.openweather.fetching || state.api.saving || state.entries.fetching
});

export default connect(mapStateToProps)(Throbber);
