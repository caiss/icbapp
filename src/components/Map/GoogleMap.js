import React from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import { getWeatherData } from '../../actions/actions';
import { connect } from 'react-redux';

const GoogleMap = props => {

  const marker = props.map.marker;

  return (
    <Map
      google={props.google}
      className={'GoogleMap'}
      zoom={14}
      initialCenter={{ lat: marker.lat || 0, lng: marker.lon || 0 }}
      onClick={(_, __, event) => props.mapClickHandler(event.latLng.lat(), event.latLng.lng()) }
    >
      { marker && <Marker position={{ lat: marker.lat || 0, lng: marker.lon || 0 }}/> }
    </Map>
  );
};


/* CONNECT */

const mapStateToProps = ({ map }) => ({ map });

const mapDispatchToProps = dispatch => ({
  mapClickHandler: (lat, lon) => dispatch(getWeatherData(lat, lon))
});

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAqJeci3JGeYhBbCJZYFZp4W3sLsDHlqr0'
})(connect(mapStateToProps, mapDispatchToProps)(GoogleMap))