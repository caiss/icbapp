import React from 'react';
import ReactModal from 'react-responsive-modal';
import {closeResultModal} from "../../actions/actions";
import columnsDefs from '../../utils/columnsDefs';
import {connect} from "react-redux";
import './Modal.scss';

const Modal = props => {

  const content = (<table className="ModalContent"><tbody>
    {columnsDefs.map(([id, key, format]) =>
      <tr key={id}>
        <th>{key}</th>
        <td>{format(props.content)}</td>
      </tr>
      )}
  </tbody></table>);

  return (
    <ReactModal
      open={props.open}
      onClose={props.closeModalHandler}
      center
    >
      <h2>OpenWeather data</h2>
      {content}
    </ReactModal>
  );
};


/* CONNECT */

const mapStateToProps = ({ modal }) => ({
  open: modal.open,
  content: modal.content
});

const mapDispatchToProps = dispatch => ({
    closeModalHandler: () => dispatch(closeResultModal())
});


export default connect(mapStateToProps, mapDispatchToProps)(Modal);