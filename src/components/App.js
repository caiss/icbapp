import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './Menu';
import GoogleMap from './Map/GoogleMap';
import Modal from './Map/Modal';
import Throbber from './Map/Throbber';
import EntriesList from './List/EntriesList';

const App = () => {

  const Home = () => (
    <>
      <GoogleMap />
      <Throbber />
      <Modal />
    </>
  );

  return (
    <div className="App">
      <Router>
        <Menu />
        <Switch>
          <Route exact path="/" component={ Home } />
          <Route exact path="/list" component={ EntriesList } />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
