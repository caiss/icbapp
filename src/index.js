import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import app from './reducers/app';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import Freeze from 'redux-freeze';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import App from './components/App';
import './index.scss';
import * as serviceWorker from './serviceWorker';


const middleware = [thunk];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
  middleware.push(Freeze);
}
const store = createStore(
  app,
  composeWithDevTools(applyMiddleware(...middleware))
);

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);


serviceWorker.unregister();
