import {
  REQUEST_WEATHER_DATA,
  RECEIVE_WEATHER_DATA,
  RECEIVE_WEATHER_DATA_ERROR,
  REQUEST_SAVE_DATA,
  RESPONSE_SAVED_DATA,
  RESPONSE_SAVED_DATA_ERROR,
  REQUEST_HISTORY_DATA,
  RECEIVE_HISTORY_DATA,
  RECEIVE_HISTORY_DATA_ERROR,
  SHOW_RESULT_MODAL,
  CLOSE_RESULT_MODAL
} from '../actions/actions';

const initialState = {
  map: {
    marker: {
      lon: 18.5384589,
      lat: 54.4940391
    },
  },
  openweather: {
    fetching: false,
    data: {},
    error: false,
    message: null,
    lastFetch: null,
    lookup: null,
  },
  api: {
    saving: false,
    error: false,
    message: null
  },
  entries: {
    fetching: false,
    error: false,
    message: null,
    data: []
  },
  modal: {
    open: false,
    content: {}
  },
};

const app = (state = initialState, action) => {

  switch (action.type) {

    case REQUEST_WEATHER_DATA:
      return {
        ...state,
        openweather: {
          ...state.openweather,
          fetching: true,
          error: false,
          message: null,
        },
      };

    case RECEIVE_WEATHER_DATA:
      return {
        ...state,
        openweather: {
          fetching: false,
          data: {...action.payload.data},
          error: false,
          message: null,
          lastFetch: action.payload.lastFetch,
          lookup: action.payload.lookup
        },
        map: {
          marker: {
            lat: action.payload.lat,
            lon: action.payload.lon,
          },
        },
      };

    case RECEIVE_WEATHER_DATA_ERROR:
      return {
        ...state,
        openweather: {
          ...state.openweather,
          fetching: false,
          error: true,
          message: action.error.message,
        },
      };

    case REQUEST_SAVE_DATA:
      return {
        ...state,
        api: {
          saving: true,
          error: false,
          message: null
        },
      };

    case RESPONSE_SAVED_DATA:
      return {
        ...state,
        api: {
          saving: false,
          error: false,
          message: null
        },
      };

    case RESPONSE_SAVED_DATA_ERROR:
      return {
        ...state,
        api: {
          saving: false,
          error: true,
          message: action.error.message,
        },
      };

    case REQUEST_HISTORY_DATA:
      return {
        ...state,
        entries: {
          fetching: true,
          error: false,
          message: null,
          data: []
        },
      };

    case RECEIVE_HISTORY_DATA:
      return {
        ...state,
        entries: {
          fetching: false,
          error: false,
          message: null,
          data: action.payload.data.data
        },
      };

    case RECEIVE_HISTORY_DATA_ERROR:
      return {
        ...state,
        entries: {
          fetching: false,
          error: true,
          message: action.error.message,
          data: []
        },
      };

    case SHOW_RESULT_MODAL:
      return {
        ...state,
        modal: {
          open: true,
          content: action.payload
        },
      };

    case CLOSE_RESULT_MODAL:
      return {
        ...state,
        modal: {
          open: false,
          content: {}
        },
      };

    default:
      return state;
  }
};

export default app;
